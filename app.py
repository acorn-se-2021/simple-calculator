import os

from flask import Flask, render_template, request
from calculator.calculator import Calculator

app = Flask(__name__)
calculator = Calculator()


@app.route('/')
def index():
    # Show the homepage
    return render_template('index.html')


@app.route('/calculate', methods=['POST'])
def calculate():
    # Evaluate the expression
    expression = request.form['expression']
    return str(calculator.evaluate(expression))


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=os.environ['PORT'])
