import anytree

from calculator.parser.parser import Parser


class Calculator:

    def __init__(self):
        self._parser = Parser()

    def evaluate(self, s):
        self._parser.load(s)
        # Try to parse the expression
        try:
            tree = self._parser.parse()
        except SyntaxError as e:
            return e.msg
        # Calculate the value of the expression
        try:
            return self.evaluate_tree(tree)
        except ZeroDivisionError:
            return 'NaN'

    def evaluate_tree(self, tree):
        root_value = tree.name
        if root_value in ['+', '-', '*', '/']:
            # We are dealing with an operation
            lhs, rhs = tree.children[0], tree.children[1]
            if root_value == '+':
                return self.evaluate_tree(lhs) + self.evaluate_tree(rhs)
            elif root_value == '-':
                return self.evaluate_tree(lhs) - self.evaluate_tree(rhs)
            elif root_value == '*':
                return self.evaluate_tree(lhs) * self.evaluate_tree(rhs)
            else:
                return self.evaluate_tree(lhs) / self.evaluate_tree(rhs)
        else:
            # We are dealing with a number
            return root_value