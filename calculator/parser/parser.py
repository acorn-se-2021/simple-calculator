from calculator.parser.utils import tokenize, is_number

from anytree import Node, RenderTree


class Parser:
    """
    A recursive decent parser for simple arithmetic expressions.
    """

    def __init__(self):
        self._tokens = []
        self._position = 0

    def load(self, s):
        """
        Loads and tokenizes a string to be parsed.

        s           - the string to be parsed.
        """
        self._tokens = tokenize(s)

    def parse(self):
        """
        Parses the loaded string.
        """
        self._position = 0
        result = self.parse_add_sub_expr()
        if self._tokens[self._position] == 'EOF':
            return result
        else:
            raise SyntaxError('Parser did not read entire input')

    def parse_factor(self):
        """
        Parses a Factor
        """
        t = self._peek_()
        if is_number(t):
            self._consume_(t)
            return Node(float(t))
        elif t == '(':
            self._consume_(t)
            expr = self.parse_add_sub_expr()
            self._consume_(')')
            return expr
        else:
            raise SyntaxError('Expected a number or (')

    def parse_mult_div_expr(self):
        """
        Parses a multiplication / division expression
        """
        expr = self.parse_factor()
        t = self._peek_()
        while t == '*' or t == '/':
            self._consume_(t)
            rhs = self.parse_factor()
            expr = Node(t, children=[expr, rhs])
            t = self._peek_()
        return expr

    def parse_add_sub_expr(self):
        """
        Parses an addition / subtraction factor
        """
        expr = self.parse_mult_div_expr()
        t = self._peek_()
        while t == '+' or t == '-':
            self._consume_(t)
            rhs = self.parse_mult_div_expr()
            expr = Node(t, children=[expr, rhs])
            t = self._peek_()
        return expr

    def _consume_(self, token):
        """
        Checks if the current token is as expected and increments
        the position counter.

        token           - the expected token.
        """
        observed_token = self._tokens[self._position]
        if observed_token == token:
            self._position += 1
        else:
            raise SyntaxError(
                f'Unexpected token \'{observed_token}\' - expected \'{token}\''
            )

    def _peek_(self):
        """
        Returns the token at the current position.
        """
        return self._tokens[self._position]
