import regex

def tokenize(s):
    """
    Splits a string into tokens.
    
    s               - the string to tokenize.
    """
    return regex.findall(r'\s*([0-9]+\.?[0-9]*|\+|-|\*|\/|\(|\))\s*', s) + ['EOF']

def is_number(s):
    """
    Checks if a string corresponds to a number.

    s               - the string to be checked.
    """
    try:
        float(s)
        return True
    except ValueError:
        return False
