from calculator.parser.utils import *


def test_tokenize():
    assert tokenize('(1.88+2)-3/4*0') == \
        ['(', '1.88', '+', '2', ')', '-', '3', '/', '4', '*', '0', 'EOF']


def test_tokenize_spaces():
    assert tokenize('(1.88    +   2)-     3/4*  0') == \
        ['(', '1.88', '+', '2', ')', '-', '3', '/', '4', '*', '0', 'EOF']


def test_tokenize_illegal():
    assert tokenize('1x+2y') == \
        ['1', '+', '2', 'EOF']


def test_tokenize_empty():
    assert tokenize('') == ['EOF']


def test_is_number():
    assert is_number('1234.1234')
    assert not is_number('1234.1234.1234')
    assert not is_number('1234x')
    assert not is_number('test')
