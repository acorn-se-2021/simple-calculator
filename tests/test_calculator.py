import pytest

from calculator.calculator import Calculator


@pytest.fixture
def calculator():
    return Calculator()


def test_evaluate_basic_arithmetic(calculator):
    assert calculator.evaluate('1+2+3+4*0') == 6
    assert calculator.evaluate('1.1-2.2/2') == 0


def test_evaluate_division_by_zero(calculator):
    assert calculator.evaluate('1/0') == 'NaN'


def test_evaluate_parenthesis(calculator):
    assert calculator.evaluate('(1+2+3+4)*0') == 0
    assert calculator.evaluate('1.1-2.2/(2-1)') == -1.1

def test_evaluate_unbalanced_parentheses(calculator):
    assert calculator.evaluate('1.1-2.2/((2-1)') == 'Unexpected token \'EOF\' - expected \')\''

def test_evaluate_empty_parentheses(calculator):
    assert calculator.evaluate('1.1-2.2/((2-1))()') == 'Parser did not read entire input'
    assert calculator.evaluate('1.1()-2.2/((2-1))()') == 'Parser did not read entire input'