from app import app


def test_home_page():
    with app.test_client() as test_client:
        response = test_client.get('/')
        assert response.status_code == 200


def test_calculation():
    with app.test_client() as test_client:
        response = test_client.post('/calculate', data={
            'expression': '1'
        })
        assert response.status_code == 200
