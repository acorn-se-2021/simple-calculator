import pytest

from calculator.parser.parser import *


@pytest.fixture
def parser():
    return Parser()


def test_load(parser):
    parser.load('1+2*0')
    assert parser._tokens == ['1', '+', '2', '*', '0', 'EOF']


def test_parse_addition(parser):
    parser.load('1+2')
    tree = parser.parse()
    assert tree.name == '+'
    assert tree.children[0].name == 1
    assert tree.children[1].name == 2


def test_parse_subtraction(parser):
    parser.load('1-2')
    tree = parser.parse()
    assert tree.name == '-'
    assert tree.children[0].name == 1
    assert tree.children[1].name == 2


def test_parse_multiplication(parser):
    parser.load('1*2')
    tree = parser.parse()
    assert tree.name == '*'
    assert tree.children[0].name == 1
    assert tree.children[1].name == 2


def test_parse_division(parser):
    parser.load('1/2')
    tree = parser.parse()
    assert tree.name == '/'
    assert tree.children[0].name == 1
    assert tree.children[1].name == 2


def test_parse_precedence(parser):
    parser.load('1+2*0')
    tree = parser.parse()
    assert tree.name == '+'


def test_parse_precedence_2(parser):
    parser.load('2/2+1')
    tree = parser.parse()
    assert tree.name == '+'


def test_parse_precedence_parentheses(parser):
    parser.load('2/(2+1)')
    tree = parser.parse()
    assert tree.name == '/'


def test_parse_unbalanced_parentheses(parser):
    parser.load('2/((2+1)')
    with pytest.raises(SyntaxError):
        parser.parse()


def test_parse_empty_parentheses(parser):
    parser.load('()2/((2+1)')
    with pytest.raises(SyntaxError):
        parser.parse()
